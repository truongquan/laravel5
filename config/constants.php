<?php
return [
    'server_path'     =>  [
        'images'     =>  [
            'products'  =>  public_path('/images/products'),
        ]
    ],
    'client_path'     =>  [
        'images'    =>  [
            'products'  =>  url('/images/products'),
       ]
    ]
];
?>
