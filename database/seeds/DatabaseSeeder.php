<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() 
	{
		//Model::unguard();
		DB::table('products')->insert(array(
			array(
				'name'=>'Galaxy Note 4', 
				'description'=>'Galaxy Note 4', 
				'price'=>'18000000', 
				'is_enable'=>'true'
				),
			array(
				'name'=>'Zenphone 4.0', 
				'description'=>'Điện thoại Zenphone 4', 
				'price'=>'2000000', 
				'is_enable'=>'true'
				),
			array(
				'name'=>'Zenphone 5', 
				'description'=>'Điện thoại Zenphone 5', 
				'price'=>'4000000',  
				'is_enable'=>'true' 
				),
			array(
				'name'=>'Zenphone 6', 
				'description'=>'Điện thoại Zenphone 6', 
				'price'=>'6000000', 
				'is_enable'=>'true'
				),
			array(
				'name'=>'Lumia 525', 
				'description'=>'Lumia 525', 
				'price'=>'2500000', 
				'is_enable'=>'true'
				),
			array(
				'name'=>'Máy tính  7 inch', 
				'description'=>'Máy tính bảng 7 inch', 
				'price'=>'16000000', 
				'is_enable'=>'true'
				),
			array(
				'name'=>'May tinh bang', 
				'description'=>'May tinh bang', 
				'price'=>'16000000', 
				'is_enable'=>'true'
				),
		));
		// $this->call('UserTableSeeder');
	}

}
