<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddProductTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('products', function($table){ 
			$table->increments('id');
			$table->string('name');
			$table->text('images');
			$table->string('description')->nullable();
			$table->integer('price');
			$table->boolean('is_enable');
			$table->timestamps(); 
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()  
	{
		
	}

}
