<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use App\Product;

// use Input;
// use App\View;

// use View;
// use Response;

// use App\Product;

class ProductController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index($sort_name = 'id', $sort_index = 'asc')
    {
        $sort_name = (in_array($sort_name, array('id', 'name', 'description', 'price'))) ? $sort_name : 'id';
        $sort_index = ($sort_index == 'asc') ? $sort_index : 'desc';

        $table = Product::orderBy($sort_name, $sort_index)->paginate(20);
        $table->setPath(route('products').'/'.$sort_name.'/'.$sort_index);
        //dd(public_path('images/products'));

        return view('product')->with('data', $table);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return View('create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $ruler = array(
            'name'          =>  'required',
            'image'         =>  'required',
            'description'   =>  'required',
            'price'         =>  'required|numeric|min:0'
        );

        $validator = Validator::make(Input::all(), $ruler);

        if ($validator->fails()) {
            echo "<br/>Error:</br>";
            return Redirect::to('products/create')->withErrors($validator)->withInput();
        } else {
            echo "success";

            $products = new Product();

            $products->name = Input::get('name');
            $products->description = Input::get('description');
            $products->price = Input::get('price');
            if (Input::get('enable') == 'on') {
                $products->is_enable = 1;
            } else {
                $products->is_enable = 0;
            }

            $file = Input::file('image');
            $file_name          = $file->getClientOriginalName();
            $destination_path   = config('constants.server_path.images.products');
            $file->move($destination_path, $file_name);
            $products->images = $file_name;

            $products->save();

            // redirect
            return Redirect::to('products');
        }      
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //get product
        $products = Product::find($id);

        //Show edit form and pass product
        return View('edit')->with('products', $products);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        $ruler = array(
            'name'          =>  'required',
            'description'   =>  'required',
            'price'         =>  'required|numeric|min:0'
        );
        $arr_source = array(
            'name'          =>  Input::get('name'),
            'description'   =>  Input::get('description'),
            'price'         =>  Input::get('price'),
            'is_enable'     =>  Input::get('enable')
        );
        $validator =  Validator::make($arr_source, $ruler);
        if($validator->fails()){
            // echo "<pre>";
            // var_dump($validator->messages());
            // echo "</pre>";
            echo "<br/>Error:</br>";
            return Redirect::to('products/' . $id . '/edit')->withErrors($validator)->withInput();
        } else {
            $products = Product::find($id);
            
            if(is_null(Input::file('image')))
            {
                $file_name = $products->images;
            } else {
                @unlink(config('constants.server_path.images.products').'/'.$products->images);
                $file = Input::file('image');
                $file_name = $file->getClientOriginalName();  
                $destination_path = config('constants.server_path.images.products');
                $file->move($destination_path, $file_name);  
            }

            $products->name = Input::get('name');
            $products->images = $file_name;
            $products->description = Input::get('description');
            $products->price = Input::get('price');
            if (Input::get('enable') == 'on') {
                $products->is_enable = 1;
            } else {
                $products->is_enable = 0;
            }

            echo "success !";
            $products->save();  //Save to database

            // redirect if success
            return Redirect::to('products');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $product = Product::find($id);

        @unlink(config('constants.server_path.images.products').'/'.$product->images);
        $product->delete();

        return Redirect::to('products');
    }

}
