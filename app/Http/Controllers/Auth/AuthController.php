<?php namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\Registrar;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

use App\User;

class AuthController extends Controller
{

    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers;

    /**
     * Create a new authentication controller instance.
     *
     * @param  \Illuminate\Contracts\Auth\Guard  $auth
     * @param  \Illuminate\Contracts\Auth\Registrar  $registrar
     * @return void
     */
    public function __construct(Guard $auth, Registrar $registrar)
    {
        $this->auth = $auth;
        $this->registrar = $registrar;

        $this->middleware('guest', ['except' => 'getLogout']);
    }

    // public function postLogin()
    // {
    //     $rules = array(
    //                 'email'     => 'required|email',
    //                 'password'  => 'required|alpha_num',
    //              );

    //     $valid = Validator::make(Input::all(), $rules);

    //     if ($valid->fails()) {
    //         return Redirect::to('auth/login')->withErrors($valid)->withInput(Input::except('password'));
    //     } else {
    //         $input = array(Input::get('email'), Input::get('password'));

    //         if (User::whereRaw('email = ? and password = ?', $input)->count() == 1) {
    //             $user = User::whereRaw('email = ? and password = ?', $input)->first();

    //             Session::put('user_login_id', $user->id);
    //             Session::put('user_name', $user->name);
    //             Session::put('user_email', $user->email);

    //             Session::flash('message', 'Login successfully!');

    //             return Redirect::to('users');
    //         }

    //         Session::flash('error', 'Your email or password is incorrect');
    //         return Redirect::to('auth/login')->withInput(Input::except('password'));
    //     }
    // }

    // public function postRegister()
    // {
    //     $rules = array(
    //                 'name'     => 'required|alpha_dash',
    //                 'email'    => 'required|email|unique:users,email',
    //                 'password' => 'required|alpha_num|confirmed',
    //              );

    //     $valid = Validator::make(Input::all(), $rules);

    //     if ($valid->fails()) {

    //         return Redirect::to('auth/register')->withErrors($valid)->withInput();
    //     } else {
    //         $user = new User;

    //         $user->name      = Input::get('name');
    //         $user->email     = Input::get('email');
    //         $user->password  = Input::get('password');

    //         $user->save();

    //         return Redirect::to('auth/login');
    //     }
    // }
}
