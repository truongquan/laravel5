<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

class UsersController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */

    public function __construct() {

        $this->middleware('auth');
    }

    public function index()
    {
        //
        return view('users.index')->with('users', User::orderBy('id', 'desc')->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
        return view('users.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        //
        $rules = array(
                    'user_name'     => 'required|alpha_dash',
                    'user_email'    => 'required|email|unique',
                    'user_password' => 'required|alpha_num|confirmed',
                 );

        $valid = Validator::make(Input::all(), $rules);

        if ($valid->fails()) {

            return Redirect::to('users/create')->withErrors($valid)->withInput();
        } else {
            $user = new User;

            $user->name      = Input::get('user_name');
            $user->email     = Input::get('user_email');
            $user->password  = Input::get('user_password');

            $user->save();

            return Redirect::to('users');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
        return view('users.edit')->with('user', User::find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        //
        $rules = array(
                    'user_name'     => 'required|alpha_dash',
                    'user_email'    => 'required|email',
                    'user_password' => 'required|alpha_num|confirmed',
                 );

        $valid = Validator::make(Input::all(), $rules);

        if ($valid->fails()) {

            return Redirect::to('users/'.$id.'/edit')->withErrors($valid)->withInput();
        } else {
            $user = User::find($id);

            $user->name      = Input::get('user_name');
            $user->email     = Input::get('user_email');
            $user->password  = Input::get('user_password');

            $user->save();

            Session::flash('message', 'Successfully updated User');
            return Redirect::to('users');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
        $user = User::find($id);
        $user->delete();

        Session::flash('message', 'Successfully Deleted User');
        return Redirect::to('users');
    }

}
