<?php namespace App;
use Eloquent;
// use Illuminate\Database\Eloquent\Model;

class Product extends Eloquent{
	public $timestamps = true;
	protected $fillable = array('name', 'description', 'price', 'is_enable');
}