@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">Home</div>

				<div class="panel-body">
					You are logged in!
					<h1>Our Features</h1>
					<a href="{{url('users')}}" class="button">Users Management</a>
					<br /><br />
					<a href="{{url('products')}}" class="button">Product Management</a>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
