@extends('app');

@section('content')

<div style="width:650px; margin-left:auto; margin-right:auto; boder: 1px solid" >
	<div class="container-fluid">
	    <div class="row">
	        <div class="col-md-8 col-md-offset-2">
	            <div class="panel panel-default">
	                <div style="margin: 35px">
		                Page Not Found<br />
						Please enter correct link ! :(

						<br/></br>
						Trang bạn mong muốn không tìm thấy.<br />      
						Hãy nhập link chính xác ! :(
	                </div>
	            </div>
	        </div>
	    </div>
	</div>
</div>


@stop