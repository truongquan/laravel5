@extends('app')

@section('content')

@if(Auth::check())

<div class="container">
<div class="products">
    <a href="{!! url('products/create') !!}" >Create Product</a>
</div>

<div class="products">
        <div class="row">
            <div class="col-lg-1">
                <p class="id"><b><a href="@if(Request::segment(3) == 'asc'){!! url('products/id/desc') !!} @else {!! url('products/id/asc') !!} @endif">Id</a></b></p>
            </div>

            <div class="col-lg-3">
                <p class="name"><b><a href="@if(Request::segment(3) == 'asc'){!! url('products/name/desc') !!} @else {!! url('products/name/asc') !!} @endif">name</a></b></p>
            </div>

            <div class="col-lg-4">
                <p class="description"><b><a href="@if(Request::segment(3) == 'asc'){!! url('products/description/desc') !!} @else {!! url('products/description/asc') !!} @endif">description</a></b></p>
            </div>

            <div class="col-lg-2">
                <b><a href="@if(Request::segment(3) == 'asc'){!! url('products/price/desc') !!} @else {!! url('products/price/asc') !!} @endif">price</a></b>
            </div>

            <div class="col-lg-2">
                <strong>Operations</strong>
            </div>

        </div>
        
    </div>

@foreach($data as $val)
    <div class="products">
        <div class="row">
            <div class="col-lg-1">
                <p class="id">{{ $val->id }}</p>
            </div>

            <div class="col-lg-3">
                <p class="name">{{ $val->name }}</p>
            </div>

            <div class="col-lg-4">
                <p class="description">{{ $val->description }}</p>
            </div>

            <div class="col-lg-2">
                <p class="price">{{ $val->price }}</p>
            </div>

            <div class="col-lg-1">
                <a href="{!! url('products/'); !!}/{{$val->id}}/edit" >Edit</a>
            </div>

            <div class="col-lg-1">
                <a class="destroy" href="{!! url('products/'.$val->id); !!}" >Delete</a>
            </div>

        </div>
    </div>
@endforeach()
    
</div>      

<form id="form-method" method="post">
    <input type="hidden" name="_token" value="{{csrf_token()}}" />
    <input type="hidden" name="_method" id="request-method" />
</form>

<div class="container">
    {!! $data->render() !!}
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $('.destroy').click(function(){
            if(confirm('Are you sure want to delete this product?'))
            {
                $('#request-method').val('DELETE');
                $('#form-method').attr("action", $(this).attr('href')).submit();    
            }
            
            return false;
        });
    });
</script>

@else
@include('auth.must_login')        
@endif
@stop
