@extends('app');

@section('content')
<script>
	function checkTextField() {
		// return true;		//Disable Javascript - Test 
		if (document.getElementById('txtName').value.trim().length == 0) {
			alert('Name can\'t blank');
			document.getElementById('txtName').focus();
			return false;
		} else if(document.getElementById('txtDescription').value.trim().length == 0) {
			alert('Description can\'t blank');
			document.getElementById('txtDescription').focus();
			return false;
		} else if (document.getElementById('txtPrice').value.trim().length == 0){
			alert('Price can\'t blank');
			document.getElementById('txtPrice').focus();
			return false;
		} else if (document.getElementById('txtPrice').value < 0){
			alert('Price can\'t negative ');
			document.getElementById('txtPrice').focus();
			return false;
		}
		return true;
	}
</script>
@if(Auth::check())
<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				
				<div class="panel-heading"><h3>Create new product</h3></div>
				<div class="panel-body">
					@if($errors->has())
						<div class="alert alert-danger">
							<h4>Please fix errors:</h4>
							@foreach($errors->all() as $error)
								{{ $error }} <br />
							@endforeach
						</div>
					@endif
					<form class="form-horizontal" method="post" action="{!! url('products') !!}" enctype="multipart/form-data" >
						<div class="form-group">
							<label class="col-md-4 control-label">Name: </label>
							<div class="col-md-6">
								<input type="text" class="form-control" id="name" value="{{ old('name') }}"
										name="name" placeholder="Enter product name">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Images: </label>
							<div class="col-md-6">
								<input type="file" name="image" />
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Description: </label>
							<div class="col-md-6">
								<input type="text" class="form-control" id="description" 
										name="description" placeholder="Enter product description" value="{{old('description')}}">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Price: </label>
							<div class="col-md-6">
								<input type="number" class="form-control" id="price"  
										onkeypress='return event.charCode >= 48 && event.charCode <= 57'
										name="price" placeholder="Enter product price" value="{{old('price')}}">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Enable: </label>
							<div class="col-md-6">
								<input type="checkbox" id="enable" name="enable" />
								<input type="hidden" name="_token" value="{{ csrf_token() }}" />
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-6 col-md-offset-4">
								<input type="submit" class="btn btn-primary" style="margin-right: 15px;"
								onclick="return checkTextField();"/>
							</div>
						</div>

					</form>
				</div>


			</div>
		</div>
	</div>
</div>
@else
@include('auth.must_login')
@endif       
@stop
