@extends('app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading"><h2>Create New User</h2></div>

                <div class="panel-body">
                    
                    <form method="post" action="{!! url('users') !!}">
                        <table cellpadding="5" cellspacing="5">
                            <tbody>
                                <tr>
                                    <td align="right">Name</td>
                                    <td align="left">
                                    <input type="text" name="user_name" value="{{ old('user_name') }}" />
                                    @if($errors->has('user_name')) <p class="error-message">{{$errors->first('user_name')}}</p>@endif
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">Email</td>
                                    <td align="left">
                                    <input type="text" name="user_email" value="{{ old('user_email') }}" />
                                    @if($errors->has('user_email')) <p class="error-message">{{$errors->first('user_email')}}</p>@endif
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">Password</td>
                                    <td align="left">
                                    <input type="password" name="user_password" />
                                    @if($errors->has('user_password')) <p class="error-message">{{$errors->first('user_password')}}</p>@endif
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">Confirm Password</td>
                                    <td align="left">
                                    <input type="password" name="user_password_confirmation" />
                                    @if($errors->has('user_password_confirmation')) <p class="error-message">{{$errors->first('user_password_confirmation')}}</p>@endif
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right"><input type="submit" value="Create" /></td>
                                    <td align="left"><input type="reset" value="Cancel" /></td>
                                </tr>
                            </tbody>
                        </table>
                        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection