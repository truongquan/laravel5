@extends('app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">User Management</div>

                <div class="panel-body">
                    <a href="{{url('users/create')}}">Create New</a>
                    <p class="message-notify">@if(Session::has('message')) {{Session::get('message')}}@endif</p>
                    <table cellpadding="10" cellspacing="5">
                        <thead>
                            <tr>
                                <th>User Name</th>
                                <th>User Email</th>
                                <th colspan="3">Operations</th>
                            </tr>
                        </thead>

                        <tbody>
                        @foreach($users as $user)
                            <tr>
                                <td>{{ $user->name }}</td>
                                <td><a href="#" class="order-email"> {{ $user->email }}</a></td>
                                <td><a href="{{ url('users/'.$user->id.'/edit') }}">Edit</a></td>
                                <td><a href="#" class="delete" rel="{{$user->id}}">Delete</a></td>
                                <td></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <form method="post" action="" id="form-delete">
                        <input type="hidden" name="_method" value="DELETE" />
                        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                    </form>
                    <script type="text/javascript">
                        $(document).ready(function(){

                            $('.delete').click(function(){

                                if(!confirm('Are you sure you want to delete this user?')) {
                                    return false;
                                }

                                var form_action = "{!!url('users')!!}" + "/" + $(this).attr('rel');
                                $('#form-delete').attr('action', form_action ).submit();

                                return false;
                            });
                        });
                    </script>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
